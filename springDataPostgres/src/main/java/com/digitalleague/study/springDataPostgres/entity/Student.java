package com.digitalleague.study.springDataPostgres.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.*;
import javax.persistence.*;
import java.util.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name="student")
@Builder
public class Student {

    @Id
    @GeneratedValue(generator = "student_seq",strategy = GenerationType.SEQUENCE)
    @SequenceGenerator(name = "student_seq", sequenceName = "student_seq", allocationSize = 1)
    @Column(name="student_id")
    private int id;

    @Column(name="first_name")
    private String firstName;

    @Column(name="last_name")
    private String lastName;

/*    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.PERSIST)
    @JoinTable(name = "student_teacher",
            joinColumns = {
                    @JoinColumn(name = "student_id")},
            inverseJoinColumns = {
                    @JoinColumn(name = "teacher_id")})
    @JsonManagedReference
    private Set<Teacher> teachers = new HashSet<>();*/

    @ManyToMany(fetch = FetchType.LAZY, cascade = {
                                                    CascadeType.PERSIST,
                                                    CascadeType.MERGE,
                                                    CascadeType.DETACH,
                                                    CascadeType.REFRESH
    })
    @JoinTable(name = "student_teacher",
            joinColumns = {@JoinColumn(name = "student_id" , updatable = false)},
            inverseJoinColumns = @JoinColumn(name = "teacher_id")
    )
    //@JsonManagedReference
    @JsonIgnore
    private List<Teacher> teachers;

    public Student(String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
    }

   public void addTeacherToStudent(Teacher teacher){
        if(teachers ==null){
            teachers = new ArrayList<>();
        }
        teachers.add(teacher);
    }

    @Override
    public String toString() {
        return "Student{" +
                "id=" + id +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                '}';
    }
}
