package com.digitalleague.study.springDataPostgres.dao;

import com.digitalleague.study.springDataPostgres.entity.Student;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;


public interface StudentRepository extends JpaRepository<Student, Integer> {
    Optional<Student> findStudentById(Integer id);
    void deleteStudentByFirstNameAndLastName(String firstName, String lastName);

}
