package com.digitalleague.study.springDataPostgres.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.*;
import javax.persistence.*;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name="teacher")
public class Teacher {

    @Id
    @GeneratedValue(generator = "teacher_seq",strategy = GenerationType.SEQUENCE)
    @SequenceGenerator(name = "teacher_seq", sequenceName = "teacher_seq", allocationSize = 1)
    @Column(name="teacher_id")
    private int id;

    @Column(name="first_name")
    private String firstName;

    @Column(name="last_name")
    private String lastName;

    @Column(name="subject")
    private String subject;

/*    @ManyToMany(mappedBy = "teachers", fetch = FetchType.LAZY)
    @JsonBackReference
    private Set<Student> students = new HashSet<>();*/

    @ManyToMany(fetch = FetchType.LAZY, cascade = {
            CascadeType.PERSIST,
            CascadeType.MERGE,
            CascadeType.DETACH,
            CascadeType.REFRESH
    })
    @JoinTable(name = "student_teacher",
            joinColumns = @JoinColumn(name = "teacher_id"),
            inverseJoinColumns = @JoinColumn(name = "student_id")
    )
/*    @ManyToMany(mappedBy = "teachers", fetch = FetchType.LAZY)*/
   // @JsonBackReference
    @JsonIgnore
    private List<Student> students;


    public Teacher(String firstName, String lastName, String subject) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.subject = subject;
    }

  /*  public void addStudentsToTeacher(Student student){
        if(students ==null){
            students = new ArrayList<>();
        }
        students.add(student);
    }*/
}
