package com.digitalleague.study.springDataPostgres.controller;

import com.digitalleague.study.springDataPostgres.entity.Teacher;
import com.digitalleague.study.springDataPostgres.service.TeacherService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;
import java.util.List;
import java.util.Optional;

/**
 * РЕСТ контроллер для работы с преподавателями
 */
@RestController
@RequiredArgsConstructor
public class TeacherController {

    private final TeacherService teacherService;

    @GetMapping("/getTeacher")
    public Optional<Teacher> getTeacher(@RequestParam Integer id) {
        return teacherService.findTeacherById(id);
    }

    @PostMapping("/addTeacher")
    public void addTeacher(@RequestBody Teacher teacher) {
        teacherService.saveTeacher(teacher);
    }

    @GetMapping("/getAllTeachers")
    public List<Teacher> getAllTeachers() {
        return teacherService.getAllTeachers();
    }

    @DeleteMapping("/deleteTeacher")
    public void deleteTeacher(@RequestParam String firstName,
                              @RequestParam String lastName){
            teacherService.deleteTeacher(firstName, lastName);
    }
}
