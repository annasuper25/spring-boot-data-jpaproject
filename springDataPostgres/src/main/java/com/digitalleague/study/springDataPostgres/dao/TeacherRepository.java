package com.digitalleague.study.springDataPostgres.dao;

import com.digitalleague.study.springDataPostgres.entity.Teacher;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface TeacherRepository extends JpaRepository<Teacher, Integer> {
    void deleteTeacherByFirstNameAndLastName(String firstName, String lastName);
    Optional<Teacher> findTeacherById(Integer id);
}
