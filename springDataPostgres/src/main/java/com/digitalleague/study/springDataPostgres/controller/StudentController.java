package com.digitalleague.study.springDataPostgres.controller;

import com.digitalleague.study.springDataPostgres.entity.Student;
import com.digitalleague.study.springDataPostgres.service.StudentService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

/**
 * РЕСТ контроллер для работы со студентами
 */
@RestController
@RequiredArgsConstructor
public class StudentController {

    private final StudentService studentService;

    @GetMapping("/getStudent")
    public Optional<Student> getStudent(@RequestParam Integer id) {
        return studentService.findStudentById(id);
    }

    @PostMapping("/addStudent")
    public void addStudent(@RequestBody Student student) {
        studentService.saveStudent(student);
    }

    @GetMapping("/getAllStudents")
    public List<Student> getAllStudents() {
        return studentService.getAllStudents();
    }

    @DeleteMapping("/deleteStudent")
    public void deleteStudent(@RequestParam String firstName,
                              @RequestParam String lastName){
        studentService.deleteStudent(firstName, lastName);
    }
}
