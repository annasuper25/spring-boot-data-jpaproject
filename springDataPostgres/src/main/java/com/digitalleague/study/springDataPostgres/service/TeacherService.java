package com.digitalleague.study.springDataPostgres.service;

import com.digitalleague.study.springDataPostgres.dao.TeacherRepository;
import com.digitalleague.study.springDataPostgres.entity.Student;
import com.digitalleague.study.springDataPostgres.entity.Teacher;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Сервис для работы с преподавателями
 */
@Service
@Transactional
@RequiredArgsConstructor
public class TeacherService {

    private final TeacherRepository teacherRepository;

    /**
     * 7. Получение одного преподавателя
     * @param id идентификатор преподавателя
     * @return сущность преподавателя
     */
    public Optional<Teacher> findTeacherById(Integer id){
        return teacherRepository.findTeacherById(id);
    }


    /**
     * 5. Добавление преподавателя
     * @param teacher сущность преподвателя
     */
    public void saveTeacher(Teacher teacher){
        List<Teacher> list = new ArrayList<>();
        list.add(teacher);
        teacherRepository.saveAll(list);
    }


    /**
     * 8. Получение всех преподавателей
     * @return массив сущностей преподавателей
     */
    public List<Teacher> getAllTeachers() {
        return teacherRepository.findAll();
    }


    /**
     * 6. Удаление преподавателя
     * @param firstName имя преподавателя
     * @param lastName фамилия преподавателя
     */
    public void deleteTeacher(String firstName, String lastName){
        teacherRepository.deleteTeacherByFirstNameAndLastName(firstName, lastName);
    }

}
