package com.digitalleague.study.springDataPostgres.service;

import com.digitalleague.study.springDataPostgres.dao.StudentRepository;
import com.digitalleague.study.springDataPostgres.dao.TeacherRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.digitalleague.study.springDataPostgres.entity.Student;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Сервис для работы со студентами
 */
@Service
@Transactional
@RequiredArgsConstructor
public class StudentService {

    private final StudentRepository studentRepository;

    /**
     * 1. Добавление студента
     * @param student сущность студента
     */
    public void saveStudent(Student student) {
        List<Student> list = new ArrayList<>();
        list.add(student);
        studentRepository.saveAll(list);
    }


    /**
     * 2. Удаление студента
     * @param firstName Имя студента
     * @param lastName Фамилия студента
     */
    public void deleteStudent(String firstName, String lastName){
        studentRepository.deleteStudentByFirstNameAndLastName(firstName, lastName);
    }


    /**
     * 3. Получение одного студента по идентификатору
     * @param id идентификатор студента
     * @return сущность студента
     */
    public Optional<Student> findStudentById(Integer id) {
        return studentRepository.findStudentById(id);
    }


    /**
     * 4. Получение всех студентов
     * @return массив сущностей студентов
     */
    public List<Student> getAllStudents() {
       return studentRepository.findAll();
    }

}
