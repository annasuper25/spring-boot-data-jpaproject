package com.digitalleague.study.springDataPostgres.service;

import com.digitalleague.study.springDataPostgres.dao.StudentRepository;
import com.digitalleague.study.springDataPostgres.dao.TeacherRepository;
import com.digitalleague.study.springDataPostgres.entity.Student;
import com.digitalleague.study.springDataPostgres.entity.Teacher;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;


/**
 * Сервис для работы со связью студентов и преподавателей
 */
@Service
@Transactional
@RequiredArgsConstructor
public class StudentTeacherService {

    private final StudentRepository studentRepository;
    private final TeacherRepository teacherRepository;

    /**
     * 9. Приписать студента к преподавателю
     * @param idStud - идентификатор студента
     * @param idTeacher - идентификатор учителя
     */
    public void addStudentToTeacher(Integer idStud,Integer idTeacher){
        Teacher teacher = teacherRepository.findTeacherById(idTeacher).orElseThrow(
                () -> new RuntimeException(
                        String.format("Не найдена сущность учителя по идентификатору=%s", idTeacher)
                )
        );

        Student student = studentRepository.findStudentById(idStud).orElseThrow(
                () -> new RuntimeException(
                        String.format("Не найдена сущность студента по идентификатору=%s", idStud)
                )
        );

        List<Student> students = teacher.getStudents();
        if(students == null){
            students = new ArrayList<>();
        }
        students.add(student);
    }


    /**
     * 10. Отписать студента от преподавателя
     * @param idStud - идентификатор студента
     * @param idTeacher - идентификатор учителя
     */
    public void delStudentFromTeacher(Integer idStud,Integer idTeacher){
        Teacher teacher = teacherRepository.findTeacherById(idTeacher).orElseThrow(
                () -> new RuntimeException(
                        String.format("Не найдена сущность учителя по идентификатору=%s", idTeacher)
                )
        );

        Student student = studentRepository.findStudentById(idStud).orElseThrow(
                () -> new RuntimeException(
                        String.format("Не найдена сущность студента по идентификатору=%s", idStud)
                )
        );

        List<Student> students = teacher.getStudents();

        if(students == null || (!(students.contains(student)))){
            throw new RuntimeException(
                    String.format("Не найден студент с идентификатором=%s у учителя с идентификатором=%s", idStud, idTeacher));
        }
        else {
            students.remove(student);
        }
    }



    /**
     * 11. Получить список студентов у преподавателя
     * @param id  - идентификатор учителя
     * @return масиив студентов
     */
    public List<Student> findStudentsByTeacher(Integer id) {
        Teacher teacher = teacherRepository.findTeacherById(id).orElseThrow(
                () -> new RuntimeException(
                        String.format("Не найдена сущность учителя по идентификатору=%s", id)
                )
        );

        List<Student> students = teacher.getStudents();

        return students;
    }


    /**
     * 12. Получить список преподавателей у студента
     * @param idStud - идентификатор студента
     * @return массив учителей
     */
    public List<Teacher> findTeachersByStudent(Integer idStud){
        Student student = studentRepository.findStudentById(idStud).orElseThrow(
                () -> new RuntimeException(
                        String.format("Не найдена сущность студента по идентификатору=%s", idStud)
                )
        );

        List<Teacher> teachers = student.getTeachers();

        return teachers;
    }
}
