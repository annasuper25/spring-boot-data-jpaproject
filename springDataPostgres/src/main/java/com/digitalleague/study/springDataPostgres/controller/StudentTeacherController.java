package com.digitalleague.study.springDataPostgres.controller;

import com.digitalleague.study.springDataPostgres.entity.Student;
import com.digitalleague.study.springDataPostgres.entity.Teacher;
import com.digitalleague.study.springDataPostgres.service.StudentTeacherService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * РЕСТ контроллер для работы со связью студентов и преподавателей
 */
@RestController
@RequiredArgsConstructor
public class StudentTeacherController {

    private final StudentTeacherService studentTeacherService;

    @PutMapping("/addStudentToTeacher/{idStud}/{idTeacher}")
    public void addStudentToTeacher(@PathVariable Integer idStud,
                                    @PathVariable Integer idTeacher) {
        studentTeacherService.addStudentToTeacher(idStud, idTeacher);
    }

    @PutMapping("/delStudentFromTeacher/{idStud}/{idTeacher}")
    public void delStudents(@PathVariable Integer idStud,
                            @PathVariable Integer idTeacher) {
        studentTeacherService.delStudentFromTeacher(idStud, idTeacher);
    }

    @GetMapping("/getTeachersByStudent")
    public List<Teacher> findTeachersByStudent(@RequestParam Integer id) {
        return studentTeacherService.findTeachersByStudent(id);
    }

    @GetMapping("/getStudentsByTeacher")
    public List<Student> findStudentsByTeacher(@RequestParam Integer id) {
        return studentTeacherService.findStudentsByTeacher(id);
    }

}
