insert into teacher(first_name, last_name, subject) VALUES ('Kirill', 'Boolychev', 'MyBatis'),
                                                          ('Nikolay', 'Kozhokar', 'Spring'),
                                                          ('Dmitriy', 'Shaplov', 'Java');

insert into student(first_name, last_name) VALUES ('Ivan', 'Ivanov');
insert into student(first_name, last_name) VALUES ('Petr', 'Petrov');

insert into student_teacher values(1,1), (1,2);
