CREATE TABLE student (
                         student_id integer PRIMARY KEY NOT NULL,
                         first_name varchar(20),
                         last_name varchar(25)
);

create sequence if not exists student_seq start 1;
alter table student alter column student_id SET default nextval('student_seq'::regclass);


CREATE TABLE teacher (
                         teacher_id integer PRIMARY KEY NOT NULL,
                         first_name varchar(20),
                         last_name varchar(25),
                         subject varchar(25)
);

create sequence if not exists teacher_seq start 1;
alter table teacher alter column teacher_id SET default nextval('teacher_seq'::regclass);


CREATE TABLE student_teacher (
                                 teacher_id integer NOT NULL,
                                 student_id integer NOT NULL,
                                 CONSTRAINT student_teacher_id PRIMARY KEY (teacher_id,student_id),
                                 FOREIGN KEY (teacher_id) REFERENCES teacher (teacher_id),
                                 FOREIGN KEY (student_id) REFERENCES student (student_id)
);